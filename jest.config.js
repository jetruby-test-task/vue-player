module.exports = {
  coverageDirectory: './build/styleguide/coverage',
  moduleNameMapper: {
    '\\.(css|jpg|png|php|htm|tpl)$': '<rootDir>/tools/jest.empty-module.js'
  }
};

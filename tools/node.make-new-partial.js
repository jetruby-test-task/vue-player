import fs from 'fs';
import path from 'path';
import { createInterface } from 'readline';

const rl = createInterface(process.stdin, process.stdout);

// folder with all blocks
const BLOCKS_DIR = path.join(__dirname, '../src/partials');

// default content for files in new block
const fileSources = {
  /**
   * HTM
   */
  // prettier-ignore
  htm:
`{% macro {blockName:capitalized}() %}
<!-- {blockName} -->
<{blockName} inline-template v-cloak>
  <section class="{blockName}">
    <div class="{blockName}__content-wrap">
      {blockName}
    </div>
  </section>
</{blockName}>
<!-- /{blockName} -->
{% endmacro %}`,

  /**
   * spec.htm
   */
  // prettier-ignore
  'spec.htm':
`{% from "partials/{blockName}/{blockName}.htm" import {blockName:capitalized} %}

{{ {blockName:capitalized}() }}`,

  /**
   * css
   */
  // prettier-ignore
  css:
`.{blockName} {
  position: relative;
  width: 100%;
  padding: 0 10px;
}

.{blockName}__content-wrap {
  position: relative;
  width: 100%;
  max-width: 1240px;
  margin: 0 auto;
}`,

  /**
   * js
   */
  // prettier-ignore
  js:
`import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('{blockName:capitalized}', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const {blockName:capitalized} = {
  computed: {
    ...mapState('{blockName:capitalized}', []),

    ...mapGetters('{blockName:capitalized}', [])
  },

  methods: {
    ...mapMutations('{blockName:capitalized}', [])
  }
};

Vue.component('{blockName:capitalized}', {blockName:capitalized});

export default {blockName:capitalized};`,

  /**
   * spec.js
   */
  // prettier-ignore
  'spec.js':
`import {blockName:capitalized} from './{blockName}';

describe('{blockName:capitalized}', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof {blockName:capitalized}).toBe('object');
  });
});`,

  /**
   * spec.md
   */
  // prettier-ignore
  'spec.md':
`{% extends "../_layouts/custom.htm" %}

[//]: HEADER

{% block header %}

# {blockName:capitalized}

{% endblock %}

[//]: COMPONENT

{% block component %}{blockName}{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

\`\`\`HTML
@@include('./gitbook/partials/{blockName}.html')
\`\`\`

{% sample lang="view" %}

### Вид

\`\`\`TWIG
@@include('./src/partials/{blockName}/{blockName}.htm')
\`\`\`

{% sample lang="model" %}

### Модель

\`\`\`JS
@@include('./src/partials/{blockName}/{blockName}.js')
\`\`\`

{% sample lang="style" %}

### Стиль

\`\`\`SCSS
@@include('./src/partials/{blockName}/{blockName}.css');
\`\`\`

{% sample lang="test" %}

### Тесты

\`\`\`JS
@@include('./src/partials/{blockName}/{blockName}.spec.js')
\`\`\`

{% endmethod %}

{% endblock %}`
};

// //////////////////////////////////////////////////////////////////////////

function validateBlockName(blockName) {
  return new Promise((resolve, reject) => {
    const isValid = /^(\d|\w|-)+$/.test(blockName);

    if (isValid) {
      resolve(isValid);
    } else {
      const errMsg =
        `ERR>>> An incorrect block name '${blockName}'\n` +
        `ERR>>> A block name must include letters, numbers & the minus symbol.`;
      reject(errMsg);
    }
  });
}

function directoryExist(blockPath, blockName) {
  return new Promise((resolve, reject) => {
    fs.stat(blockPath, notExist => {
      if (notExist) {
        resolve();
      } else {
        reject(`ERR>>> The block '${blockName}' already exists.`);
      }
    });
  });
}

function createDir(dirPath) {
  return new Promise((resolve, reject) => {
    fs.mkdir(dirPath, err => {
      if (err) {
        reject(`ERR>>> Failed to create a folder '${dirPath}'`);
      } else {
        resolve();
      }
    });
  });
}

function createFiles(blocksPath, blockName) {
  const promises = [];
  Object.keys(fileSources).forEach(ext => {
    const fileSource = fileSources[ext]
      .replace(/\{blockName}/g, blockName)
      .replace(
        /\{blockName:capitalized}/g,
        blockName
          .toLowerCase()
          .replace(/(\b|-)\w/g, name => name.toUpperCase().replace(/-/, ''))
      );
    const filename = `${blockName}.${ext}`;
    const filePath = path.join(blocksPath, filename);

    promises.push(
      new Promise((resolve, reject) => {
        fs.writeFile(filePath, fileSource, 'utf8', err => {
          if (err) {
            reject(`ERR>>> Failed to create a file '${filePath}'`);
          } else {
            // создание пустой папки для изображений
            fs.mkdir(`${blocksPath}/images`, err => {
              if (err) {
                reject(`ERR>>> Failed to create a folder '${blocksPath}'`);
              } else {
                resolve();
              }
            });
            resolve();
          }
        });
      })
    );
  });

  return Promise.all(promises);
}

function getFiles(blockPath) {
  return new Promise((resolve, reject) => {
    fs.readdir(blockPath, (err, files) => {
      if (err) {
        reject(`ERR>>> Failed to get a file list from a folder '${blockPath}'`);
      } else {
        resolve(files);
      }
    });
  });
}

function printErrorMessage(errText) {
  console.log(errText);
  rl.close();
}

// //////////////////////////////////////////////////////////////////////////

function initMakeBlock(candidateBlockName) {
  const blockNames = candidateBlockName.trim().split(/\s+/);

  const makeBlock = blockName => {
    const blockPath = path.join(BLOCKS_DIR, blockName);

    return validateBlockName(blockName)
      .then(() => directoryExist(blockPath, blockName))
      .then(() => createDir(blockPath))
      .then(() => createFiles(blockPath, blockName))
      .then(() => getFiles(blockPath))
      .then(files => {
        const line = '-'.repeat(48 + blockName.length);
        console.log(line);
        console.log(
          `The block has just been created in 'src/partials/${blockName}'`
        );
        console.log(line);

        // Displays a list of files created
        files.forEach(file => console.log(file));

        rl.close();
      });
  };

  if (blockNames.length === 1) {
    return makeBlock(blockNames[0]);
  }

  const promises = blockNames.map(name => makeBlock(name));
  return Promise.all(promises);
}

// //////////////////////////////////////////////////////////////////////////
//
// Start here
//

// Command line arguments
const blockNameFromCli = process.argv
  .slice(2)
  // join all arguments to one string (to simplify the capture user input errors)
  .join(' ');

// If the user pass the name of the block in the command-line options
// that create a block. Otherwise - activates interactive mode
if (blockNameFromCli !== '') {
  initMakeBlock(blockNameFromCli).catch(printErrorMessage);
} else {
  rl.setPrompt('Block(s) name: ');
  rl.prompt();
  rl.on('line', line => {
    initMakeBlock(line).catch(printErrorMessage);
  });
}

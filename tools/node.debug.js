const ExtractTextPlugin = require('extract-text-webpack-plugin');
const getSlug = require('speakingurl');
const glob = require('glob-all');
const webpack = require('webpack');

/*
|-------------------------------------------------------------------------------
| Вспомогательные функции
|-------------------------------------------------------------------------------
*/

/**
 * Натуральная сортировка
 * @param {[String]} arr
 * @returns {[String]}
 */
function naturalSort(arr) {
  return [].slice.call(arr).sort((a, b) => {
    const ax = [];
    const bx = [];

    a.replace(/(\d+)|(\D+)/g, (_, $1, $2) => {
      ax.push([$1 || Infinity, $2 || '']);
    });
    b.replace(/(\d+)|(\D+)/g, (_, $1, $2) => {
      bx.push([$1 || Infinity, $2 || '']);
    });

    while (ax.length && bx.length) {
      const an = ax.shift();
      const bn = bx.shift();
      const nn = an[0] - bn[0] || an[1].localeCompare(bn[1]);
      if (nn) return nn;
    }

    return ax.length - bx.length;
  });
}

/**
 * Формирование объекта из названий страниц и ссылок на них.
 * @returns {[String]}
 */
function getPagesObject() {
  const pagesUrls = naturalSort(
    glob
      .sync(['./src/pages/**'], {
        nodir: true,
        matchBase: false
      })
      .map(element =>
        getSlug(
          element.replace(/\.\/src\/pages\//, '').replace(/\.htm/, '.html'),
          {
            mark: true,
            uric: true
          }
        )
      )
  );

  const pagesNames = naturalSort(
    glob
      .sync(['./src/pages/**'], {
        nodir: true,
        matchBase: false
      })
      .map(element =>
        element.replace(/\.\/src\/pages\//, '').replace(/\.htm/, '')
      )
  );

  const pagesObject = pagesUrls.reduce(
    (accumulator, value, index) =>
      Object.assign(accumulator, {
        [value]: pagesNames[index]
      }),
    {}
  );

  return pagesObject;
}

console.log(getPagesObject());

const rimraf = require('rimraf');

function removeDirectory(path) {
  rimraf(path, () => {
    console.log(`Директория ${path} удалена`);
  });
}

removeDirectory('./build');

import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('PlayerSearch', {
  namespaced: true,

  state() {
    return {
      query: ''
    };
  },

  getters: {},

  mutations: {
    /**
     * Обновление запроса на сортировку
     * @param {*} state
     * @param {String} query
     */
    updateQuery(state, query) {
      state.query = query;
    }
  }
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const PlayerSearch = {
  template: require('./player-search.htm'),

  computed: {
    ...mapState('PlayerSearch', ['query']),
    ...mapState('PlayerContainer', ['isFocusOnSearch']),

    ...mapGetters('PlayerSearch', []),

    message: {
      get() {
        return this.query;
      },
      set(query) {
        this.updateQuery(query);
      }
    }
  },

  methods: {
    ...mapMutations('PlayerSearch', ['updateQuery']),
    ...mapMutations('PlayerContainer', ['setFocusOnSearchState'])
  }
};

Vue.component('PlayerSearch', PlayerSearch);

export default PlayerSearch;

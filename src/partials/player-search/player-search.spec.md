{% extends "../_layouts/custom.htm" %}

[//]: HEADER

{% block header %}

# PlayerSearch

{% endblock %}

[//]: COMPONENT

{% block component %}player-search{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
@@include('./gitbook/partials/player-search.html')
```

{% sample lang="view" %}

### Вид

```TWIG
@@include('./src/partials/player-search/player-search.htm')
```

{% sample lang="model" %}

### Модель

```JS
@@include('./src/partials/player-search/player-search.js')
```

{% sample lang="style" %}

### Стиль

```SCSS
@@include('./src/partials/player-search/player-search.css');
```

{% sample lang="test" %}

### Тесты

```JS
@@include('./src/partials/player-search/player-search.spec.js')
```

{% endmethod %}

{% endblock %}
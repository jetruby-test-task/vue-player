{% extends "../_layouts/custom.htm" %}

[//]: HEADER

{% block header %}

# PlayerContainer

{% endblock %}

[//]: COMPONENT

{% block component %}player-container{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
@@include('./gitbook/partials/player-container.html')
```

{% sample lang="view" %}

### Вид

```TWIG
@@include('./src/partials/player-container/player-container.htm')
```

{% sample lang="model" %}

### Модель

```JS
@@include('./src/partials/player-container/player-container.js')
```

{% sample lang="style" %}

### Стиль

```SCSS
@@include('./src/partials/player-container/player-container.css');
```

{% sample lang="test" %}

### Тесты

```JS
@@include('./src/partials/player-container/player-container.spec.js')
```

{% endmethod %}

{% endblock %}
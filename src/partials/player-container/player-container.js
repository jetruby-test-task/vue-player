import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('PlayerContainer', {
  namespaced: true,

  state() {
    return {
      isFocusOnSearch: false
    };
  },

  getters: {},

  mutations: {
    /**
     * Установка состояния поискового компонента активен/неактивен
     * @param {*} state
     * @param {Boolean} focus
     */
    setFocusOnSearchState(state, focus) {
      state.isFocusOnSearch = focus;
    }
  }
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const PlayerContainer = {
  template: require('./player-container.htm'),

  computed: {
    ...mapState('PlayerContainer', ['isFocusOnSearch']),

    ...mapGetters('PlayerContainer', [])
  },

  methods: {
    ...mapMutations('PlayerContainer', ['setFocusOnSearchState'])
  }
};

Vue.component('PlayerContainer', PlayerContainer);

export default PlayerContainer;

{% extends "../_layouts/custom.htm" %}

[//]: HEADER

{% block header %}

# PlayerTracklist

{% endblock %}

[//]: COMPONENT

{% block component %}player-tracklist{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
@@include('./gitbook/partials/player-tracklist.html')
```

{% sample lang="view" %}

### Вид

```TWIG
@@include('./src/partials/player-tracklist/player-tracklist.htm')
```

{% sample lang="model" %}

### Модель

```JS
@@include('./src/partials/player-tracklist/player-tracklist.js')
```

{% sample lang="style" %}

### Стиль

```SCSS
@@include('./src/partials/player-tracklist/player-tracklist.css');
```

{% sample lang="test" %}

### Тесты

```JS
@@include('./src/partials/player-tracklist/player-tracklist.spec.js')
```

{% endmethod %}

{% endblock %}
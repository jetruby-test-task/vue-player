import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import jsmediatags from 'jsmediatags/dist/jsmediatags.min';
import moment from 'moment';
import PerfectScrollbar from 'perfect-scrollbar';
import 'perfect-scrollbar/css/perfect-scrollbar.css';
import 'moment-duration-format';
import 'animate.css';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('PlayerTracklist', {
  namespaced: true,

  state() {
    return {
      currentTrackId: null,
      tracklist: require('./player-tracklist.json')
    };
  },

  getters: {},

  mutations: {
    /**
     * Установка id текущего трека
     * @param {*} state
     * @param {Number} index - индекс трека
     */
    setCurrentTrackId(state, index) {
      state.currentTrackId = index;
    },

    /**
     * Получение метаданных из треков
     * @param {*} state
     */
    getMetadata(state) {
      const newState = state;
      newState.tracklist.forEach(item => {
        const audio = document.createElement('audio');
        audio.setAttribute('src', item.link);
        audio.addEventListener('loadedmetadata', () => {
          Vue.set(item, 'duration', audio.duration);
        });
        jsmediatags.read(document.location.href + item.link, {
          onSuccess(response) {
            Vue.set(item, 'metadata', response);
          },
          onError(error) {
            console.error(error);
          }
        });
      });
    }
  }
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const PlayerTracklist = {
  data() {
    return {
      scrollbar: undefined
    };
  },

  template: require('./player-tracklist.htm'),

  computed: {
    ...mapState('PlayerTracklist', ['tracklist', 'currentTrackId']),
    ...mapState('PlayerControls', ['audio']),
    ...mapState('PlayerSearch', ['query']),

    ...mapGetters('PlayerTracklist', []),

    // Отсортированный треклист
    filteredTracklist() {
      return this.query.length >= 3
        ? this.tracklist.filter(
          item =>
            item.metadata.tags.artist
              .toLowerCase()
              .indexOf(this.query.toLowerCase()) !== -1 ||
              item.metadata.tags.title
                .toLowerCase()
                .indexOf(this.query.toLowerCase()) !== -1
        )
        : this.tracklist;
    }
  },

  methods: {
    ...mapMutations('PlayerTracklist', ['getMetadata', 'setCurrentTrackId']),
    ...mapMutations('PlayerControls', ['setAudioState']),
    ...mapMutations('PlayerSearch', ['updateQuery']),

    /**
     * Поиск индекса трека в треклисте исходя из индекса отсортированного треклиста
     * @param {Number} filteredIndex
     * @returns {Number}
     */
    findIndexInTracklist(filteredIndex) {
      return this.tracklist.findIndex(
        (item, index) =>
          this.tracklist[index] === this.filteredTracklist[filteredIndex]
      );
    }
  },

  filters: {
    formatDuration(value) {
      return moment.duration(parseInt(value, 10), 'seconds').format('mm:ss', {
        trim: false
      });
    }
  },

  created() {
    // Получаем метаданные
    this.getMetadata();
  },

  mounted() {
    // Инициализируем скроллбар
    this.scrollbar = new PerfectScrollbar(
      '.player-tracklist[data-scrollbar]',
      {
        suppressScrollX: true
      }
    );
  }
};

Vue.component('PlayerTracklist', PlayerTracklist);

export default PlayerTracklist;

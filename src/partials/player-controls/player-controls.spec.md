{% extends "../_layouts/custom.htm" %}

[//]: HEADER

{% block header %}

# PlayerControls

{% endblock %}

[//]: COMPONENT

{% block component %}player-controls{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
@@include('./gitbook/partials/player-controls.html')
```

{% sample lang="view" %}

### Вид

```TWIG
@@include('./src/partials/player-controls/player-controls.htm')
```

{% sample lang="model" %}

### Модель

```JS
@@include('./src/partials/player-controls/player-controls.js')
```

{% sample lang="style" %}

### Стиль

```SCSS
@@include('./src/partials/player-controls/player-controls.css');
```

{% sample lang="test" %}

### Тесты

```JS
@@include('./src/partials/player-controls/player-controls.spec.js')
```

{% endmethod %}

{% endblock %}
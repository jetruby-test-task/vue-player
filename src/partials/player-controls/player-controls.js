/* eslint 'import/no-extraneous-dependencies': 0 */
/* eslint 'import/no-webpack-loader-syntax': 0 */
/* eslint 'import/extensions': 0 */
/* eslint 'import/no-unresolved': 0 */
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import particles from 'exports-loader?particlesJS=window.particlesJS,window.pJSDom!particles.js';
import moment from 'moment';
import 'moment-duration-format';
import particlesConfig from './player-controls.particles-config';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('PlayerControls', {
  namespaced: true,

  state() {
    return {
      audio: {
        el: null,
        src: null,
        playState: false,
        volume: 0.6,
        currentTime: 0,
        userSetTime: null,
        duration: null,
        mute: false,
        lastVolume: null
      }
    };
  },

  getters: {},

  mutations: {
    /**
     * Создание элемента "аудио"
     * @param {*} state
     */
    setAudio(state) {
      state.audio.el = document.getElementById('player-controls-audio');
      state.audio.el.volume = state.audio.volume;
    },

    /**
     * Изменение состояния трека
     * @param {* } state
     * @param {Object} payload
     */
    setAudioState(state, payload) {
      Object.keys(payload).forEach(item => {
        Vue.set(state.audio, item, payload[item]);
      });
    }
  }
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const PlayerControls = {
  template: require('./player-controls.htm'),

  data() {
    return {
      plugins: {
        particles: {
          enable: true,
          options: particlesConfig
        }
      }
    };
  },

  computed: {
    ...mapState('PlayerControls', ['audio']),
    ...mapState('PlayerTracklist', ['tracklist', 'currentTrackId']),

    ...mapGetters('PlayerControls', []),
    ...mapGetters('PlayerTracklist', ['filteredTracklist'])
  },

  methods: {
    ...mapMutations('PlayerControls', ['setAudio', 'setAudioState']),
    ...mapMutations('PlayerTracklist', ['setCurrentTrackId']),

    /**
     * Смена трека в плейлисте
     * @param {'next'|'prev'} direction - следующий/предыдущий
     */
    changeTrack(direction) {
      if (direction === 'prev') {
        if (this.currentTrackId === null) {
          this.setCurrentTrackId(this.tracklist.length - 1);
        } else if (
          this.currentTrackId <= this.tracklist.length - 1 &&
          this.currentTrackId > 0
        ) {
          this.setCurrentTrackId(this.currentTrackId - 1);
        } else {
          this.setCurrentTrackId(this.tracklist.length - 1);
        }
      } else if (direction === 'next') {
        if (this.currentTrackId === null) {
          this.setCurrentTrackId(0);
        } else if (this.currentTrackId < this.tracklist.length - 1) {
          this.setCurrentTrackId(this.currentTrackId + 1);
        } else {
          this.setCurrentTrackId(0);
        }
      } else {
        throw new Error('Передан неверный параметр');
      }
    }
  },

  watch: {
    currentTrackId(newValue) {
      this.setAudioState({
        playState: true,
        src: this.tracklist[newValue].link,
        duration: this.tracklist[newValue].duration
      });
      setTimeout(() => {
        this.audio.el.play();
      }, 0);
    },

    'audio.playState': function(newValue) {
      setTimeout(() => {
        if (newValue) {
          return this.audio.el.play();
        }
        return this.audio.el.pause();
      }, 0);
    },

    'audio.src': function(newValue, oldValue) {
      if (newValue !== oldValue) {
        this.audio.src = newValue;
        this.audio.userSetTime = 0;
      }
    },

    'audio.volume': function(newValue, oldValue) {
      if (newValue !== oldValue) {
        this.audio.volume = parseFloat(newValue, 10);
        this.audio.el.volume = this.audio.volume;
        this.audio.mute = this.audio.volume === 0;
      }
    },

    'audio.mute': function(newValue) {
      if (newValue) {
        this.audio.lastVolume = parseFloat(this.audio.volume) + 0.01;
        this.audio.volume = 0;
      } else {
        this.audio.volume = this.audio.lastVolume;
      }
    },

    'audio.userSetTime': function(newValue) {
      this.audio.el.currentTime = parseInt(newValue, 10);
    },

    'audio.currentTime': function(newValue) {
      if (newValue === this.audio.duration) {
        setTimeout(() => {
          this.changeTrack('next');
        }, 1000);
      }
    }
  },

  filters: {
    formatDuration(value) {
      return moment.duration(parseInt(value, 10), 'seconds').format('mm:ss', {
        trim: false
      });
    }
  },

  mounted() {
    // Создание аудиоэлемента
    this.setAudio();

    // Инициализация декора "частицы"
    if (this.plugins.particles.enable) {
      particles.particlesJS('player-controls', this.plugins.particles.options);
    }
  }
};

Vue.component('PlayerControls', PlayerControls);

export default PlayerControls;

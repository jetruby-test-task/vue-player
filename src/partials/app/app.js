/* eslint  no-unused-vars: 0 */

import Vue from 'vue/dist/vue';
import bowser from 'bowser';
import 'babel-polyfill';
import objectFitImages from 'object-fit-images';
import 'normalize.css/normalize.css';
import store from './app.store';

Vue.config.productionTip = false;
Vue.config.performance = process.env.NODE_ENV === 'dev';
Vue.config.devtools = process.env.NODE_ENV === 'dev';
Vue.config.ignoredElements = ['vue-ignore', 'noindex', 'code', 'pre'];

const cq = require('cq-prolyfill')({ preprocess: true });

const App = new Vue({
  el: '#app',

  name: 'App',

  delimiters: ['[[', ']]'],

  store,

  data: {
    device: {},
    isShowWireframes:
      window.location.search.substr(1).search(/show-wireframes=true/) !== -1,
    options: {
      serviceWorkers: {
        enable: process.env.NODE_ENV !== 'dev'
      },
      disableUserSelect: {
        enable: false
      }
    },
    plugins: {
      bowser: {
        enable: true
      },
      objectFitImages: {
        enable: true,
        options: {
          watchMQ: true
        }
      }
    },
    noScroll: false
  },

  methods: {
    /**
     * Регистрация сервис-воркера
     */
    registerServiceWorkers() {
      if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('service-worker.js');
      }
    },

    /**
     * Запрет пользователю копировать содержимое
     */
    disableUserSelect() {
      document.ondragstart = false;
      document.onselectstart = false;
      document.oncontextmenu = false;
    },

    /**
     * Получение сведений о браузере и системе пользователя
     */
    bowser() {
      function detectBrowserEngine() {
        let browser;
        if (bowser.blink) {
          browser = 'blink';
        } else if (bowser.webkit) {
          browser = 'webkit';
        } else if (bowser.gecko) {
          browser = 'gecko';
        } else if (bowser.msie) {
          browser = 'msie';
        } else if (bowser.msedge) {
          browser = 'msedge';
        }
        return browser;
      }

      function detectDeviceType() {
        let deviceType;
        if (bowser.mobile) {
          deviceType = 'mobile';
        } else if (bowser.tablet) {
          deviceType = 'tablet';
        } else {
          deviceType = 'laptop';
        }
        return deviceType;
      }

      function detectOs() {
        let os;
        if (bowser.mac) {
          os = 'mac';
        } else if (bowser.windows) {
          os = 'windows';
        } else if (bowser.windowsphone) {
          os = 'windowsphone';
        } else if (bowser.linux) {
          os = 'linux';
        } else if (bowser.chromeos) {
          os = 'chromeos';
        } else if (bowser.android) {
          os = 'android';
        } else if (bowser.ios) {
          os = 'ios';
        } else if (bowser.blackberry) {
          os = 'blackberry';
        } else if (bowser.firefoxos) {
          os = 'firefoxos';
        } else if (bowser.webos) {
          os = 'webos';
        } else if (bowser.bada) {
          os = 'bada';
        } else if (bowser.tizen) {
          os = 'tizen';
        } else if (bowser.sailfish) {
          os = 'sailfish';
        }
        return os;
      }

      this.device.browser = bowser.name;
      this.device.browserVersion = bowser.version;
      this.device.browserEngine = detectBrowserEngine();
      this.device.type = detectDeviceType();
      this.device.os = detectOs();
      this.device.osVersion = bowser.osversion;
    },

    /**
     * Инициализация полифила для object-fit
     */
    objectFitImages() {
      objectFitImages(null, this.plugins.objectFitImages.options);
    }
  },

  beforeMount() {
    if (this.plugins.bowser.enable) {
      this.bowser();
    }
  },

  mounted() {
    if (this.options.serviceWorkers.enable) {
      this.registerServiceWorkers();
    }

    if (this.options.disableUserSelect.enable) {
      this.disableUserSelect();
    }

    if (this.plugins.bowser.enable) {
      this.bowser();
    }

    if (this.plugins.objectFitImages.enable) {
      this.objectFitImages();
    }
  }
});
